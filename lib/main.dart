import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Заметки',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: NoteListPage(),
    );
  }
}
class Note {
  final String title;
  final String description;
  Note({
    required this.title,
    required this.description,
  });
}
class NoteListPage extends StatefulWidget {
  @override
  _NoteListPageState createState() => _NoteListPageState();
}
class _NoteListPageState extends State<NoteListPage> {
  List<Note> notes = [];
  @override
  void initState() {
    super.initState();
    loadNotes();
  }
  Future loadNotes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      notes = (prefs.getStringList('notes') ?? []).map((note) {
        List<String> noteData = note.split(',');
        return Note(
          title: noteData[0],
          description: noteData[1],
        );
      }).toList();
    });
  }
  Future saveNotes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> savedNotes = notes.map((note) {
      return '${note.title},${note.description}';
    }).toList();
    await prefs.setStringList('notes', savedNotes);
  }
  void addNote() async {
    Note? result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoteEditPage()),
    );
    if (result != null) {
      setState(() {
        notes.add(result);
        saveNotes();
      });
    }
  }
  void editNote(int index) async {
    Note? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteEditPage(note: notes[index]),
      ),
    );
    if (result != null) {
      setState(() {
        notes[index] = result;
        saveNotes();
      });
    }
  }
  void deleteNote(int index) async {
    bool confirm = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Удалить заметку?'),
          content: Text('Вы действительно хотите удалить эту заметку?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text('Отмена'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Удалить'),
            ),
          ],
        );
      },
    );
    if (confirm != null && confirm) {
      setState(() {
        notes.removeAt(index);
        saveNotes();
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Заметки'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.0,
        ),
        itemCount: notes.length,
        itemBuilder: (context, index) {
          String title = notes[index].title;
          String description = notes[index].description;
          return Card(
            child: ListTile(
              title: Text(title),
              subtitle: Text(description),
              onTap: () => editNote(index),
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () => deleteNote(index),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addNote,
        child: Icon(Icons.add),
      ),
    );
  }
}
class NoteEditPage extends StatefulWidget {
  final Note? note;
  NoteEditPage({this.note});
  @override
  _NoteEditPageState createState() => _NoteEditPageState();
}
class _NoteEditPageState extends State<NoteEditPage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.note != null) {
      titleController.text = widget.note!.title;
      descriptionController.text = widget.note!.description;
    }
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  void saveNote() {
    Note note = Note(
      title: titleController.text,
      description: descriptionController.text,
    );
    Navigator.pop(context, note);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Редактировать заметку'),
        actions: [
          IconButton(
            onPressed: saveNote,
            icon: Icon(Icons.check),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                controller: titleController,
                decoration: InputDecoration(
                  labelText: 'Заголовок',
                ),
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: descriptionController,
                maxLines: 8,
                decoration: InputDecoration(
                  labelText: 'Текст',
                ),
              ),
              SizedBox(height: 16.0),
            ],
          ),
        ),
      ),
    );
  }
}
